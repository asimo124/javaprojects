import javax.swing. *;
import javax.swing.border.LineBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.*;
import java.awt. *;
import java.io.File;


/**
 * Created by IntelliJ IDEA.
 * User: Butt
 * Date: 4/7/2006
 * Time: 0:12:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class SelectFolderForm extends JFrame {
    private JButton button1;
    public JTree tree;
    public JPanel mainPanel;

    public SelectFolderForm() throws HeadlessException {

        super ("Project Directory");













    }

    public void loadFiles() {

        File dir = new File(SearchCommand.ProjectPath);
        tree = new JTree(addNodes(dir));
        tree.setRootVisible(false);
        tree.setShowsRootHandles(true);
        tree.setBorder(new LineBorder(new Color(0, 0, 0)));
        tree.getSelectionModel().setSelectionMode(
                TreeSelectionModel.SINGLE_TREE_SELECTION);

        tree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                treeValueChanged(e);
            }
        });

        tree.setCellRenderer(new FileTreeCellRenderer());

        JScrollPane treeView = new JScrollPane(tree);
        treeView.setPreferredSize(new Dimension(500, 768));

        setContentPane(treeView);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }


    /*/
    public static void main (String [] args) {
        new SelectFolderForm ();
    }
    //*/

    public Dimension getMinimumSize() {
        return new Dimension(500, 768);
    }
    public Dimension getPreferredSize() {
        return new Dimension(500, 768);
    }

    private void treeValueChanged(TreeSelectionEvent e) {

        TreePath path = e.getPath();
        String paths = path.toString();

        String[] pathArr2 = paths.split(", ");
        int lastIndex = pathArr2.length - 1;
        String[] pathArr = pathArr2[lastIndex].split("]");
        String curFolderPath = pathArr[0];
        File newFile = new File(curFolderPath);

        if (newFile.isDirectory()) {

            SearchCommand.FolderPath = curFolderPath;

            NavFrames.MainSearchForm = new MainSearchForm();

            if (NavFrames.MainSearchFrame != null) {

                NavFrames.MainSearchFrame.setVisible(true);

            } else {

                NavFrames.MainSearchFrame = new JFrame("Win Grep");
                NavFrames.MainSearchFrame.setContentPane(NavFrames.MainSearchForm.panelMain);
                NavFrames.MainSearchFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                NavFrames.MainSearchFrame.pack();
                NavFrames.MainSearchFrame.setVisible(true);
                NavFrames.MainSearchFrame.setLocationRelativeTo(null);

                NavFrames.MainSearchFrame.setLocation(NavFrames.MainSearchFrame.getLocationOnScreen().x, NavFrames.MainSearchFrame.getLocationOnScreen().y - 300);

                SearchCommand.MainSearchX = NavFrames.MainSearchForm.lblTempBottom.getLocationOnScreen().x;
                SearchCommand.MainSearchY = NavFrames.MainSearchForm.lblTempBottom.getLocationOnScreen().y;
            }
        }
    }

    public DefaultMutableTreeNode addNodes(File dir) {
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(dir);
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                node.add(addNodes(file));
            } else {
                node.add(new DefaultMutableTreeNode(file));
            }
        }
        return node;
    }

    public class FileTreeCellRenderer extends DefaultTreeCellRenderer {
        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            if (value instanceof DefaultMutableTreeNode) {
                value = ((DefaultMutableTreeNode)value).getUserObject();
                if (value instanceof File) {
                    value = ((File) value).getName();
                }
            }
            return super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        }
    }
}