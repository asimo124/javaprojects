import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.awt.*;

/**
 * Created by AHawley on 5/12/2017.
 */
public class SearchResultsForm extends JFrame {

    private JPanel panelMain;
    private JTable      table;
    private JScrollPane scrollPane;

    private int extraTopPadding = 20;
    private int leftMargin = -27;

    /**
     * constructor - show table as JPanel
     *
     * @param title
     * @throws HeadlessException
     */
    public SearchResultsForm(String title) throws HeadlessException {
        super(title);

        SearchResultsTableModel useModel = new SearchResultsTableModel();
        useModel.data = SearchCommand.completeDataModel;

        table.setModel(useModel);

        //table.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        Dimension dim = new Dimension();
        dim.setSize(15, 7);
        table.setRowHeight(22);
        table.setIntercellSpacing(dim);

        setContentPane(panelMain);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);

        setLocation(this.getX(), SearchCommand.MainSearchY + SearchCommand.MainSearchHeight + this.extraTopPadding);
        setVisible(true);

    }

    /**
     * main function - if you want to run this as the starting piont of the application
     *
     * @param args
     */
    public static void main(String[] args) {
        //new SearchResultsForm("AssignFields");
    }
}
