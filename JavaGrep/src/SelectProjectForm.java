import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AHawley on 5/3/2017.
 */
public class SelectProjectForm {

    public JPanel panelMain;
    public JComboBox cboSelProject;
    private JButton btnNext;

    public List<String> ProjectPaths = new ArrayList<>();
    public String CurProjectPath = "";

    public boolean clickedNext = false;

    public SelectProjectForm() {

        cboSelProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CurProjectPath = ProjectPaths.get(cboSelProject.getSelectedIndex());
                SearchCommand.ProjectPath = CurProjectPath;
            }
        });
        btnNext.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (clickedNext == false) {

                    clickedNext = true;
                    if (NavFrames.SelectFolderFrame != null) {
                        //NavFrames.SelectFolderFrame.dispose();
                    }
                    NavFrames.SelectFolderForm = new SelectFolderForm();
                    NavFrames.SelectFolderForm.loadFiles();





                    clickedNext = false;
                }
            }
        });
    }

    public static void main(String args[]) {
        NavFrames.SelectProjectForm = new SelectProjectForm();
        NavFrames.SelectProjectFrame = new JFrame("Win Grep");
        NavFrames.SelectProjectFrame.setContentPane(NavFrames.SelectProjectForm.panelMain);
        NavFrames.SelectProjectFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        NavFrames.SelectProjectFrame.pack();
        NavFrames.SelectProjectFrame.setVisible(true);
        NavFrames.SelectProjectFrame.setLocationRelativeTo(null);

        NavFrames.SelectProjectForm.loadProjects();
        NavFrames.SelectProjectForm.CurProjectPath = NavFrames.SelectProjectForm.ProjectPaths.get(0);
        SearchCommand.ProjectPath = NavFrames.SelectProjectForm.CurProjectPath;
    }

    void loadProjects() {

        List<String> ProjectNames = new ArrayList<String>();
        ProjectPaths = new ArrayList<>();
        File file = new File("C:\\Temp\\vb_grep_dropdown_paths.txt");
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null; ) {
                String[] lineArr = line.split("\\t");

                ProjectPaths.add(lineArr[0]);
                ProjectNames.add(lineArr[1]);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        cboSelProject.setModel(new DefaultComboBoxModel(ProjectNames.toArray()));
    }
}
