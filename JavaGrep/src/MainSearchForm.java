import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AHawley on 5/12/2017.
 */
public class MainSearchForm extends JFrame {

    public  JPanel panelMain;
    public JComboBox cboSearchType;
    public JComboBox cboFileType;
    public JCheckBox justFileNamesCheckBox;
    public JTextField txtKeyword;
    private JButton btnSearch;
    public JLabel lblTempBottom;
    private JButton btnBackProject;
    private JButton btnBackFolder;
    public JLabel lblTemp;

    private boolean searchClicked = false;

    public MainSearchForm() {

        JFrame self = this;

        loadDropdowns();
        cboSearchType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                SearchCommand.SearchType = cboSearchType.getSelectedItem().toString();
            }
        });
        cboFileType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                SearchCommand.FileType = cboFileType.getSelectedItem().toString();
            }
        });
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                if (searchClicked == false) {
                    searchClicked = true;
                    if (SearchCommand.SearchType == "Text in Files") { // if is text search

                        System.out.println("is text in files");

                        if (txtKeyword.getText() != "") { // if entered search keyword

                            System.out.println("keyword entered");

                            List<File> FilesSearched = new ArrayList<>();

                            System.out.println("fileType: " + SearchCommand.FileType);
                            if (SearchCommand.FileType == "ALL") {

                                System.out.println("all files");
                                SearchCommand.FileType = "*";

                                File file = new File(SearchCommand.FolderPath);
                                FilesSearched = addFiles(null, file);

                            } else {

                                File file = new File(SearchCommand.FolderPath);
                                FilesSearched = addFiles(null, file, SearchCommand.FileType.toLowerCase());
                            }

                            int counter = 1;
                            System.out.println("onlyfilenames: " + SearchCommand.OnlyFileNames);

                            if (SearchCommand.OnlyFileNames == false) {  // if want to see found lines

                                SearchResultsTableModel TableModel = new SearchResultsTableModel();
                                List<List<Object>> resultsArr = new ArrayList<>();
                                for (File getFile : FilesSearched) {


                                    int lines_count2 = 1;
                                    int lines_count_all = 1;
                                    try (BufferedReader br = new BufferedReader(new FileReader(getFile))) {

                                        int modelCount = 0;

                                        for (String line; (line = br.readLine()) != null; ) {

                                            if (modelCount == 0) {
                                                //System.out.println(line);
                                            }

                                            int line_index = counter - 1;


                                            if (line.matches(".*" + txtKeyword.getText() + ".*")) {

                                                List<Object> eachObject = new ArrayList<>();
                                                eachObject.add(getFile.getAbsolutePath());
                                                eachObject.add(line + ":" + lines_count2);

                                                resultsArr.add(eachObject);

                                                //TableModel.setValueAt(getFile.getAbsolutePath(), modelCount, 0);
                                                //TableModel.setValueAt(line + ":" + lines_count, modelCount, 1);

                                                modelCount++;
                                                lines_count2++;
                                                counter++;

                                            }

                                            lines_count_all++;
                                        }

                                    } catch (IOException eIO) {

                                        System.out.println(eIO.getMessage());
                                    }


                                }
                                searchClicked = false;
                                SearchCommand.dataModel = resultsArr;
                                SearchCommand.convertDataModel();

                                System.out.println("open search results form");

                                if (NavFrames.SearchResultsFrame != null) {
                                    NavFrames.SearchResultsFrame.dispose();
                                }
                                NavFrames.SearchResultsFrame = new SearchResultsForm("Search Results");



                            } else {  // if is filename search

                                /*/
                                int counter3 = 1;
                                int lines_count3 = 1;

                                List<String> foundFiles = new List<String>();
                                foreach (String getFile in files)
                                {

                                    int lines_count4 = 1;
                                    StreamReader file =
                                            new StreamReader(getFile);
                                    string line;
                                    while ((line = file.ReadLine()) != null)
                                    {
                                        int line_index = counter - 1;
                                        if (System.Text.RegularExpressions.Regex.IsMatch(line, txtSearchTerm.Text))
                                        {
                                            String get_cur_path = getFile.Replace(cur_path, "");
                                            get_cur_path = get_cur_path.Replace("\\", "/");

                                            var match = foundFiles.FirstOrDefault(stringToCheck => stringToCheck.Contains(get_cur_path));
                                            if (match == null)
                                            {
                                                //if (lines_count > 1)
                                                //{
                                                Array.Resize(ref this.SearchResultsFrame, counter3);
                                                //}

                                                this.SearchResultsFrame[lines_count3 - 1] = new SearchResult { Path = get_cur_path, FullPath = getFile, LineNumber = "0", Results = "", OpenNotePad = "Notepad", OpenPHPStorm = "PHPStorm", OpenPath = "Folder" };
                                                foundFiles.Add(get_cur_path);
                                                lines_count3++;
                                                counter3++;
                                            }
                                        }

                                    }
                                    file.Close();
                                }
                                //*/
                            }

                            /*/
                            dgvSearchResults.DataSource = this.SearchResultsFrame;

                            //*/
                        } else {
                            /*/
                            this.SearchResultsFrame = new SearchResult[0];
                            dgvSearchResults.DataSource = this.SearchResultsFrame;
                            //*/
                        }

                        /*/
                        lblLoading.Visible = false;
                        this.Update();
                        //*/
                    } else {
                        /*/
                        lblLoading.Visible = true;
                        lblLoading.Text = "Loading...";
                        this.Update();

                        String cur_path = "";
                        String cur_path2 = this.paths[cboPaths.SelectedIndex];


                        String[] curPathArr;
                        curPathArr = cur_path2.Split('\\');
                        int lastCurPathIndex = curPathArr.Length - 1;


                        if (tvCurrentPath.SelectedNode != null)
                        {
                            String cur_node = tvCurrentPath.SelectedNode.FullPath.ToString();
                            Debug.WriteLine("cur_Node: " + cur_node);

                            cur_path = cur_path2 + cur_node.Replace(curPathArr[lastCurPathIndex], "\\");
                        }
                        else
                        {
                            cur_path = cur_path2;
                        }
                        Debug.WriteLine("cur_path: " + cur_path);

                        String cur_file_type = cboFileTypes.SelectedItem.ToString();
                        if (cur_file_type == "ALL")
                        {
                            cur_file_type = "*";
                        }

                        this.SearchResultsFrame = new SearchResult[0];
                        dgvSearchResults.DataSource = this.SearchResultsFrame;
                        List<string> files = Directory.GetFiles(cur_path, "*." + cur_file_type, SearchOption.AllDirectories).ToList();

                        int counter = 1;
                        int lines_count = 1;

                        List<String> foundFiles = new List<String>();
                        foreach (String getFile in files)
                        {



                            String get_cur_path = getFile.Replace(cur_path, "");
                            get_cur_path = get_cur_path.Replace("\\", "/");

                            if (get_cur_path.IndexOf(txtSearchTerm.Text, StringComparison.OrdinalIgnoreCase) >= 0)
                            {
                                var match = foundFiles.FirstOrDefault(stringToCheck => stringToCheck.Contains(get_cur_path));
                                if (match == null)
                                {
                                    //if (lines_count > 1)
                                    //{
                                    Array.Resize(ref this.SearchResultsFrame, counter);
                                    //}

                                    this.SearchResultsFrame[lines_count - 1] = new SearchResult { Path = get_cur_path, FullPath = getFile, LineNumber = "0", Results = "", OpenNotePad = "Notepad", OpenPHPStorm = "PHPStorm", OpenPath = "Folder" };
                                    foundFiles.Add(get_cur_path);
                                    lines_count++;
                                    counter++;
                                }

                            }

                        }


                        dgvSearchResults.DataSource = this.SearchResultsFrame;


                        lblLoading.Visible = false;
                        this.Update();
                        //*/
                    }
                    //*/
                }


            }
        });
        justFileNamesCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                AbstractButton absB = (AbstractButton) e.getSource();
                ButtonModel bMod = absB.getModel();
                boolean selected = bMod.isSelected();
                if (!selected) {
                    SearchCommand.OnlyFileNames = true;
                } else {
                    SearchCommand.OnlyFileNames = false;
                }

            }
        });
        btnBackProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                NavFrames.SelectProjectFrame.setVisible(true);
            }
        });
    }


    public List<File> addFiles(List<File> files, File dir)
    {
        if (files == null) {
            files = new ArrayList<File>();
        }

        if (!dir.isDirectory()) {
            files.add(dir);
            return files;
        }

        for (File file : dir.listFiles()) {
            addFiles(files, file);
        }
        return files;
    }

    public List<File> addFiles(List<File> files, File dir, String extension)
    {
        if (files == null) {
            files = new ArrayList<File>();
        }

        if (!dir.isDirectory()) {

            if (dir.getName().endsWith(extension)) {
                files.add(dir);
            }
            return files;
        }

        for (File file : dir.listFiles()) {
            addFiles(files, file, extension);
        }
        return files;
    }


    private void loadDropdowns() {

        List<String> SearchTypes = new ArrayList<>();
        SearchTypes.add("Text in Files");
        SearchTypes.add("File Names");

        cboSearchType.setModel(new DefaultComboBoxModel(SearchTypes.toArray()));

        List<String> FileTypes = new ArrayList<>();
        FileTypes.add("ALL");
        FileTypes.add("PHP");
        FileTypes.add("TWIG");
        FileTypes.add("JS");
        FileTypes.add("HTM");
        FileTypes.add("CSS");
        FileTypes.add("SQL");
        FileTypes.add("PY");
        FileTypes.add("JAVA");
        FileTypes.add("XML");

        cboFileType.setModel(new DefaultComboBoxModel(FileTypes.toArray()));
    }
}
