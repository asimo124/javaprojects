import javax.swing.*;

/**
 * Created by AHawley on 5/12/2017.
 */
public class NavFrames {

    public static JFrame SelectProjectFrame = null;
    public static JFrame SelectFolderFrame = null;
    public static JFrame MainSearchFrame = null;
    public static JFrame SearchResultsFrame = null;

    public static SelectProjectForm SelectProjectForm = null;
    public static SelectFolderForm SelectFolderForm = null;
    public static MainSearchForm MainSearchForm = null;
    public static SearchResultsForm SearchResultsForm = null;

    public static void setSelectFolderVisible() {

        SelectProjectFrame.setVisible(false);
        if (MainSearchFrame != null) {
            MainSearchFrame.setVisible(false);
        }
        if (SearchResultsFrame != null) {
            SearchResultsFrame.setVisible(false);
        }
    }
}
