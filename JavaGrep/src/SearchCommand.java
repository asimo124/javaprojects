import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AHawley on 5/3/2017.
 */
public class SearchCommand {

    public static String SearchFileType = "";

    public static String ProjectPath = "";
    public static String FolderPath = "";
    public static String SearchType = "Text in Files";
    public static String FileType = "ALL";
    public static Boolean OnlyFileNames = false;
    public static List<List<Object>> dataModel = new ArrayList<>();
    public static Object[][] completeDataModel = null;

    public static int MainSearchX = 0;
    public static int MainSearchY = 0;
    public static int MainSearchHeight = 0;

    public static void convertDataModel() {

        completeDataModel = new Object[dataModel.size()][2];
        int i = 0;
        for (List<Object> eachItem: dataModel) {
            completeDataModel[i][0] = eachItem.get(0);
            completeDataModel[i][1] = eachItem.get(1);
            i++;
        }
    }
}