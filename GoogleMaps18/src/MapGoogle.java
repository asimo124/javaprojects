
import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Hello world!
 *
 */
public class MapGoogle
{
    public static void main( String[] args ) {

    }

    public String getAddress(String address) {
        JSONObject retVal = queryGoogle(address);
        String retAddress = "";
        try {
            JSONArray results = retVal.getJSONArray("results");
            ArrayList<String> list = new ArrayList<String>();
            for(int i = 0 ; i < results.length() ; i++){
                list.add(results.getJSONObject(i).getString("formatted_address"));
            }
            retAddress = list.get(0).toString();
        } catch (JSONException e) {

            e.printStackTrace();
        }
        return retAddress;
    }
    public LatLong getLatLong(String address) {

        JSONObject retVal = queryGoogle(address);
        String retAddress = "";
        LatLong retLonLat = new LatLong();
        try {
            JSONArray results = retVal.getJSONArray("results");
            ArrayList<Double> listLat = new ArrayList<Double>();
            ArrayList<Double> listLon = new ArrayList<Double>();
            for(int i = 0 ; i < results.length() ; i++) {
                JSONObject geometry = results.getJSONObject(i).getJSONObject("geometry");
                JSONObject location = geometry.getJSONObject("location");
                Double lat = location.getDouble("lat");
                Double lon = location.getDouble("lng");
                listLat.add(lat);
                listLon.add(lon);
            }
            double lat = listLat.get(0);
            double lon = listLon.get(0);
            retLonLat = new LatLong();
            retLonLat.setLat(lat);
            retLonLat.setLon(lon);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return retLonLat;
    }
    protected JSONObject queryGoogle(String address) {
        HttpGet httpGet = new HttpGet("https://maps.google.com/maps/api/geocode/json?key=AIzaSyD2qjUJqjojYD__9Laj9Vu1v3PJ7PUSYqU&address=" + URLEncoder.encode(address));
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
            // exception
        } catch (IOException e) {
            // exception
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}