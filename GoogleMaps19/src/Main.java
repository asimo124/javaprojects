/**
 * Created by dev5 on 7/25/2016.
 */
public class Main {

    public static void main(String[] args) {

        String address = "";
        if (args.length == 0) {
            address = "4518 winlock san antonio, TX 78228";
        } else {
            address = args[0];
        }

        MapGoogle Map = new MapGoogle();

        String retAddress = Map.getAddress(address);

        LatLong retLatLong = Map.getLatLong(address);

        System.out.println("address: " + retAddress);
        System.out.println("lat: " + retLatLong.getLat());
        System.out.println("lon: " + retLatLong.getLon());
    }
}
